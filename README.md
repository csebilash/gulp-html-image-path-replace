# gulp-html-image-path-replace


Replace the `src` of the images in your HTML files.

## Example

##### gulpfile.js

```js
var gulp = require('gulp');
var replaceHtmlImages = require('gulp-html-image-path-replace');

gulp.task('html-images', function(done){
	gulp.src([
		config.publicDir + '/app/views/landing/*',
	]).pipe(replaceHtmlImages({path: "https://www.shopup.com.bd"}))
	.pipe(gulp.dest(config.publicDir + '/app/views/landing/'))
	.on('end', function(){
    	done();
  	});
});
```


##### Before:

```html
<html>
  <head>
  </head>
  <body>
    <img src="sample1.png" />
    <img src="subdirectory/sample2.png" />
    <img src="https://www.wikipedia.org/static/favicon/wikipedia.ico" />
...
```


##### After:

```html
<html>
  <head>
  </head>
  <body>
    <img src="build/images/sample.png">
    <img src="build/images/subdirectory/sample2.png" />
    <img src="https://www.wikipedia.org/static/favicon/wikipedia.ico" />
...
```


### License

MIT © lagartoflojo