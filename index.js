var gutil = require('gulp-util');
var through = require('through2');
var cheerio = require('cheerio');
var path = require('path');


module.exports = function (opts) {
    var path = "/";
    if (opts && "path" in opts) 
        path = opts.path + "/";
    
    return through.obj(function (file, enc, cb) {
        if (file.isNull()) {
            this.push(file);
            return cb(null, file);
        }
        if (file.isStream()) {
            this.emit('error', new gutil.PluginError('gulp-html-image-path-replace', 'Streaming not supported'));
            return cb(null, file);
        }
        if (file.isBuffer()) {
            var src = file.contents.toString(); 
            var $ = cheerio.load(String(file.contents));
            
            $('img').each(function () {
                if (this.attr('src')) {
                    var image = this.attr('src');
                    if (image) {
                        arr.push(image);
                        image = image.split('/');
                        this.attr('src', path + image[image.length - 1]);
                        newArr.push(this.attr('src'));
                    }
                }
            });

            var output = $.html();
            file.contents = new Buffer(output);
            return cb(null, file);
        }
    });
};
